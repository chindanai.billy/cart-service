const Constant = require("../constant");
const _ = require("underscore");
const moment = require("moment");
const PdfPrinter = require("pdfmake");
const nodemailer = require("nodemailer");
const { bahttext } = require("bahttext");
const commaNumber = require("comma-number");
const wordcut = require("wordcut");
const { Upload } = require("@aws-sdk/lib-storage");
const S3 = require("../s3/s3.js");

const getCart = (id, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const findCart = await prisma.CartTable.findFirst({
        where: {
          customer_table_id: +id,
          status: false,
        },
        select: {
          id: true,
          discount_code: true,
          status: true,
          CustomerTable: {
            select: {
              id: true,
              user_name: true,
              payer_information_table: {
                select: {
                  id: true,
                  email: true,
                  country: true,
                  first_name: true,
                  last_name: true,
                  address: true,
                  optional: true,
                  subdistrict: true,
                  city: true,
                  province: true,
                  postal_code: true,
                  phone: true,
                },
              },
            },
          },
          TaxInformationTable: {
            select: {
              id: true,
              name: true,
              id_card: true,
              address: true,
              phone: true,
            },
          },
          UserTable: {
            select: {
              id: true,
              user_name: true,
            },
          },
          CartItemTable: {
            orderBy: {
              created_at: "desc",
            },
            select: {
              id: true,
              qty: true,
              cart_table_id: true,
              metadata: true,
              ref_id: true,
            },
          },
        },
      });

      if (!_.isEmpty(findCart)) {
        return resolve({
          statusCode: 200,
          result: findCart,
        });
      } else {
        return resolve({
          statusCode: 500,
          result: {},
        });
      }
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const manageItemFromCart = (
  status_user,
  customer_table_id,
  ref_id,
  color,
  type,
  item_id,
  cart_id,
  prisma
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionManageItemFromCart = async () => {
        return await prisma.$transaction(async (tx) => {
          const findCart = await tx.CartTable.findFirst({
            where: {
              customer_table_id: +customer_table_id,
              status: false,
            },
          });

          if (_.isEmpty(findCart)) {
            const createCart = await tx.CartTable.create({
              data: {
                customer_table_id: +customer_table_id,
                status_user: status_user,
                discount_code: "0",
              },
            });

            let checkRef = ref_id.slice(0, 1);

            if (checkRef === "P") {
              const findProduct = await tx.ProductTable.findFirst({
                where: {
                  ref_id: ref_id,
                },
              });

              let meta = {
                id: findProduct.id,
                product_name: findProduct.product_name,
                product_description: findProduct.product_description,
                category_table_id: findProduct.category_table_id,
                collection_table_id: findProduct.collection_table_id,
                price: findProduct.price,
                size_fit: findProduct.size_fit,
                weight: findProduct.weight,
                detail: findProduct.detail,
                care_guide: findProduct.care_guide,
                show_image: findProduct.show_image,
                hover_image: findProduct.hover_image,
                color: findProduct.color,
                new_arrival: findProduct.new_arrival,
                pre_order: findProduct.pre_order,
                status: findProduct.status,
                color_select: color,
              };

              const createItem = await tx.CartItemTable.create({
                data: {
                  ref_id: ref_id,
                  qty: 1,
                  cart_table_id: createCart.id,
                  metadata: meta,
                },
              });

              if (!_.isEmpty(createItem)) {
                return resolve({
                  statusCode: 200,
                  result: createItem,
                });
              }
            } else {
              const createItem = await tx.CartItemTable.create({
                data: {
                  ref_id: ref_id,
                  qty: 1,
                  cart_table_id: createCart.id,
                  metadata: color,
                },
              });

              if (!_.isEmpty(createItem)) {
                return resolve({
                  statusCode: 200,
                  result: createItem,
                });
              }
            }
          } else {
            let checkRef = ref_id.slice(0, 1);

            if (checkRef === "P") {
              const findItem = await tx.CartItemTable.findFirst({
                where: {
                  OR: [
                    {
                      id: +item_id,
                    },
                    {
                      AND: {
                        cart_table_id: +cart_id,
                        ref_id: ref_id,
                        metadata: {
                          path: ["color_select", "color"],
                          equals: color.color,
                        },
                      },
                    },
                  ],
                },
              });

              if (_.isEmpty(findItem)) {
                const findProduct = await tx.ProductTable.findFirst({
                  where: {
                    ref_id: ref_id,
                  },
                });

                let meta = {
                  id: findProduct.id,
                  product_name: findProduct.product_name,
                  product_description: findProduct.product_description,
                  category_table_id: findProduct.category_table_id,
                  collection_table_id: findProduct.collection_table_id,
                  price: findProduct.price,
                  size_fit: findProduct.size_fit,
                  weight: findProduct.weight,
                  detail: findProduct.detail,
                  care_guide: findProduct.care_guide,
                  show_image: findProduct.show_image,
                  hover_image: findProduct.hover_image,
                  color: findProduct.color,
                  new_arrival: findProduct.new_arrival,
                  pre_order: findProduct.pre_order,
                  status: findProduct.status,
                  color_select: color,
                };

                const createItem = await tx.CartItemTable.create({
                  data: {
                    ref_id: ref_id,
                    qty: 1,
                    cart_table_id: +findCart.id,
                    metadata: meta,
                  },
                });

                if (!_.isEmpty(createItem)) {
                  return resolve({
                    statusCode: 200,
                    result: createItem,
                  });
                }
              }

              let checkColor = false;
              if (findItem.metadata.color_select.color == color.color) {
                checkColor = true;
              }

              console.log(
                "🚀 ~ returnawaitprisma.$transaction ~ !_.isEmpty(findItem) && checkColor:",
                !_.isEmpty(findItem) && checkColor
              );
              if (!_.isEmpty(findItem) && checkColor) {
                const updateQty = await tx.CartItemTable.update({
                  where: {
                    id: +findItem.id,
                  },
                  data: {
                    ref_id: ref_id,
                    qty:
                      type === Constant.MINUS
                        ? (findItem.qty -= 1)
                        : (findItem.qty += 1),
                    cart_table_id: +findCart.id,
                  },
                });

                if (!_.isEmpty(updateQty)) {
                  return resolve({
                    statusCode: 200,
                    result: updateQty,
                  });
                }
              }
            } else {
              const createItem = await tx.CartItemTable.create({
                data: {
                  ref_id: ref_id,
                  qty: 1,
                  cart_table_id: +findCart.id,
                  metadata: color,
                },
              });

              if (!_.isEmpty(createItem)) {
                return resolve({
                  statusCode: 200,
                  result: createItem,
                });
              }
            }
          }
        });
      };

      await transactionManageItemFromCart();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const removeItemFromCart = (id, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionDeleteItem = async () => {
        return await prisma.$transaction(async (tx) => {
          const deleteItem = await tx.CartItemTable.delete({
            where: {
              id: +id,
            },
          });

          if (!_.isEmpty(deleteItem)) {
            return resolve({
              statusCode: 200,
              result: deleteItem,
            });
          }
        });
      };

      await transactionDeleteItem();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const finishCart = (
  token,
  cart_table_id,
  payer_data,
  discount_code,
  tax_data,
  prisma,
  h
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionManageItemFromCart = async () => {
        return await prisma.$transaction(async (tx) => {
          // console.log(
          //   "🚀 ~ payer_data:",
          //   payer_data.payer_information_table[0]
          // );
          await tx.PayerInformationTable.update({
            where: {
              id: payer_data.payer_information_table[0].id,
            },
            data: {
              email: payer_data.payer_information_table[0].email,
              country: payer_data.payer_information_table[0].country,
              first_name: payer_data.payer_information_table[0].first_name,
              last_name: payer_data.payer_information_table[0].last_name,
              address: payer_data.payer_information_table[0].address,
              optional: payer_data.payer_information_table[0].optional,
              subdistrict: payer_data.payer_information_table[0].subdistrict,
              city: payer_data.payer_information_table[0].city,
              province: payer_data.payer_information_table[0].province,
              postal_code: payer_data.payer_information_table[0].postal_code,
              phone: payer_data.payer_information_table[0].phone,
            },
          });

          if (tax_data.select_tax === true) {
            await tx.TaxInformationTable.create({
              data: {
                cart_table_id: cart_table_id,
                name: tax_data.name,
                id_card: tax_data.id_card,
                address: tax_data.address,
                phone: tax_data.phone,
              },
            });
          }

          await tx.CartTable.update({
            where: {
              id: +cart_table_id,
            },
            data: {
              customer_table_id: payer_data.id,
              discount_code: discount_code.toString(),
              status: true,
              status_tax_invoice: tax_data.select_tax,
            },
          });

          const createCartItem = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +cart_table_id,
            },
            select: {
              qty: true,
              metadata: true,
            },
          });
          let sum = createCartItem.reduce((acc, product) => {
            return acc + product.qty * product.metadata.price;
          }, 0);

          const omise = require("omise")({
            secretKey: Constant.SECRET_KEY,
            omiseVersion: "2019-05-29",
          });
          let orderId =
            "order_" +
            moment().format("YYYY") +
            "_" +
            moment().format("MMDDHHmmssSSS") +
            "N";
          const createOrder = await tx.OrderTransactionTable.create({
            data: {
              order_id: orderId,
              transaction_id:
                "tran-padding-" +
                moment().format("YYYY") +
                "-" +
                moment().format("MMDDHHmmssSSS"),
              cart_table_id: cart_table_id,
              delivery_status: "...",
              status: "padding",
              total_price: parseFloat(sum.toString().replace(/,/g, "")),
              charge_id:
                "charge-padding-" +
                moment().format("YYYY") +
                "-" +
                moment().format("MMDDHHmmssSSS"),
            },
          });

          const createCharges = await new Promise((resolve, reject) => {
            omise.charges.create(
              {
                description:
                  "Charge for customer :" +
                  payer_data.payer_information_table[0].first_name +
                  " " +
                  payer_data.payer_information_table[0].last_name,
                amount: 2000,
                currency: "thb",
                card: token,
              },
              async (err, resp) => {
                if (err) {
                  console.log("🚀 ~ returnnewPromise ~ err:", err);
                  return reject(err);
                }
                return resolve(resp);
              }
            );
          });

          let paid = +createCharges.amount / 100;

          const finishOrder = await tx.OrderTransactionTable.update({
            where: {
              id: +createOrder.id,
            },
            data: {
              transaction_id: createCharges.transaction,
              cart_table_id: cart_table_id,
              delivery_status: "...",
              status: createCharges.status,
              created_order_at: new Date(createCharges.created_at),
              paid_at: new Date(createCharges.paid_at),
              total_price: parseFloat(sum.toString().replace(/,/g, "")),
              paid_price: parseFloat(paid.toString().replace(/,/g, "")),
              charge_id: createCharges.id,
            },
          });

          const findCartFilter = await prisma.CartTable.findMany({
            orderBy: {
              created_at: "desc",
            },
            where: {
              customer_table_id: +payer_data.id,
            },
            select: {
              id: true,
              CartItemTable: {
                select: {
                  id: true,
                  ref_id: true,
                  qty: true,
                  metadata: true,
                },
              },
              OrderTransactionTable: {
                select: {
                  id: true,
                  order_id: true,
                  charge_id: true,
                  transaction_id: true,
                  delivery_status: true,
                  status: true,
                  total_price: true,
                  paid_price: true,
                  created_order_at: true,
                  paid_at: true,
                },
              },
              OrderManualTransactionTable: {
                select: {
                  id: true,
                  order_id: true,
                  transaction_id: true,
                  delivery_status: true,
                  status: true,
                  total_price: true,
                  paid_price: true,
                  created_order_at: true,
                  paid_at: true,
                },
              },
            },
          });

          let resultType1 = findCartFilter.filter(
            (item) => item.OrderTransactionTable.length !== 0
          );

          const findCartManual = await prisma.CartTable.findMany({
            orderBy: {
              created_at: "desc",
            },
            where: {
              customer_table_id: +payer_data.id,
            },
            select: {
              id: true,
              CartItemTable: {
                select: {
                  id: true,
                  ref_id: true,
                  qty: true,
                  metadata: true,
                },
              },
              OrderManualTransactionTable: {
                select: {
                  id: true,
                  order_id: true,
                  transaction_id: true,
                  delivery_status: true,
                  status: true,
                  total_price: true,
                  paid_price: true,
                  created_order_at: true,
                  paid_at: true,
                },
              },
            },
          });
          let resultType2 = findCartManual.filter(
            (item) => item.OrderManualTransactionTable.length !== 0
          );

          let sumNormal = resultType1.reduce((total, object) => {
            return total + object.OrderTransactionTable[0].total_price;
          }, 0);

          let sumQr = resultType2.reduce((total, object) => {
            return total + object.OrderManualTransactionTable[0].total_price;
          }, 0);

          let score = sumNormal + sumQr;

          const findLevel = await tx.levelTable.findFirst({
            where: {
              level_score: {
                lte: score == 0 ? parseFloat(sum) : score,
              },
            },
            orderBy: {
              level_score: "desc",
            },
          });

          await tx.CustomerTable.update({
            where: {
              id: payer_data.id,
            },
            data: {
              level_table_id: findLevel.id,
            },
          });

          const findCart = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +cart_table_id,
            },
          });

          for (let itemCart of findCart) {
            let checkRef = itemCart.ref_id.slice(0, 1);

            if (checkRef === "P") {
              const findById = await tx.StockTable.findFirst({
                where: {
                  product_code: itemCart.ref_id,
                  color_name: itemCart.metadata.color_select.name,
                },
                select: {
                  id: true,
                  product_code: true,
                  product_name: true,
                  qty: true,
                  color_name: true,
                  status: true,
                },
              });

              await tx.StockMovementTable.create({
                data: {
                  stock_table_id: +findById.id,
                  status: 2,
                  qty: +itemCart.qty,
                  order_id: createOrder.order_id,
                },
              });

              await tx.StockTable.update({
                where: {
                  id: +findById.id,
                },
                data: {
                  qty:
                    +itemCart.qty >= +findById.qty
                      ? 0
                      : +findById.qty - +itemCart.qty,
                  status: +itemCart.qty >= +findById.qty ? 2 : 1,
                },
              });
            }
          }
          if (!_.isEmpty(finishOrder)) {
            const findCartItem = await tx.CartItemTable.findMany({
              where: {
                cart_table_id: +cart_table_id,
              },
              select: {
                qty: true,
                metadata: true,
                ref_id: true,
              },
            });

            let createData = [];
            findCartItem.map((item, i) => {
              createData.push({
                numberno: i + 1,
                description:
                  item.ref_id.slice(0, 1) === "P"
                    ? item.metadata.product_name
                    : item.metadata.shape_name,
                quantity: item.qty,
                price: item.metadata.price,
              });
            });

            if (tax_data.select_tax === true) {
              console.log(
                "🚀 ~ returnawaitprisma.$transaction ~ createData:",
                createData
              );

              fullTax(
                tax_data.name,
                tax_data.address,
                tax_data.id_card,
                tax_data.phone,
                orderId,
                moment().add(543, "years").format("YYYY-MM-DD"),
                createData,
                payer_data.payer_information_table[0].email,
                h,
                prisma,
                +createOrder.id,
                1
              );
            } else {
              console.log(
                "🚀 ~ returnawaitprisma.$transaction ~ createData:else",
                createData
              );

              shortTax(
                tax_data.name,
                tax_data.address,
                tax_data.id_card,
                tax_data.phone,
                orderId,
                moment().add(543, "years").format("YYYY-MM-DD"),
                createData,
                payer_data.payer_information_table[0].email,
                h,
                prisma,
                +createOrder.id,
                1
              );
            }

            return resolve({
              statusCode: 200,
              result: finishOrder,
            });
          }
        });
      };

      await transactionManageItemFromCart();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const finishCartQrCode = (
  slip,
  cart_table_id,
  payer_data,
  discount_code,
  tax_data,
  prisma
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionManageItemFromCart = async () => {
        return await prisma.$transaction(async (tx) => {
          await tx.PayerInformationTable.update({
            where: {
              id: payer_data.payer_information_table[0].id,
            },
            data: {
              email: payer_data.payer_information_table[0].email,
              country: payer_data.payer_information_table[0].country,
              first_name: payer_data.payer_information_table[0].first_name,
              last_name: payer_data.payer_information_table[0].last_name,
              address: payer_data.payer_information_table[0].address,
              optional: payer_data.payer_information_table[0].optional,
              subdistrict: payer_data.payer_information_table[0].subdistrict,
              city: payer_data.payer_information_table[0].city,
              province: payer_data.payer_information_table[0].province,
              postal_code: payer_data.payer_information_table[0].postal_code,
              phone: payer_data.payer_information_table[0].phone,
            },
          });

          if (tax_data.select_tax === true) {
            await tx.TaxInformationTable.create({
              data: {
                cart_table_id: cart_table_id,
                name: tax_data.name,
                id_card: tax_data.id_card,
                address: tax_data.address,
                phone: tax_data.phone,
              },
            });
          }

          await tx.CartTable.update({
            where: {
              id: +cart_table_id,
            },
            data: {
              customer_table_id: payer_data.id,
              discount_code: discount_code.toString(),
              status: true,
              status_tax_invoice: tax_data.select_tax,
            },
          });

          const createCartItem = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +cart_table_id,
            },
            select: {
              qty: true,
              metadata: true,
            },
          });
          let sum = createCartItem.reduce((acc, product) => {
            return acc + product.qty * product.metadata.price;
          }, 0);

          let orderId =
            "order_" +
            moment().format("YYYY") +
            "_" +
            moment().format("MMDDHHmmssSSS") +
            "N";
          const createOrder = await tx.OrderManualTransactionTable.create({
            data: {
              order_id: orderId,
              transaction_id:
                "tran-padding-" +
                moment().format("YYYY") +
                "-" +
                moment().format("MMDDHHmmssSSS"),
              cart_table_id: +cart_table_id,
              delivery_status: "...",
              status: "padding",
              total_price: parseFloat(sum.toString().replace(/,/g, "")),
              created_order_at: new Date(),
              paid_at: new Date(),
              paid_price: parseFloat(sum.toString().replace(/,/g, "")),
              slip: [slip],
            },
          });

          if (!_.isEmpty(createOrder)) {
            return resolve({
              statusCode: 200,
              result: createOrder,
            });
          }
        });
      };

      await transactionManageItemFromCart();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const finishCartGuess = (
  token,
  payer_data,
  discount_code,
  tax_data,
  cart_item,
  tex_status,
  prisma,
  h
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionManageItemFromCart = async () => {
        return await prisma.$transaction(async (tx) => {
          const createPayer = await tx.PayerInformationGuestTable.create({
            data: {
              email: payer_data.payer_information_guest_table.email,
              country: payer_data.payer_information_guest_table.country,
              first_name: payer_data.payer_information_guest_table.first_name,
              last_name: payer_data.payer_information_guest_table.last_name,
              address: payer_data.payer_information_guest_table.address,
              optional: payer_data.payer_information_guest_table.optional,
              subdistrict: payer_data.payer_information_guest_table.subdistrict,
              city: payer_data.payer_information_guest_table.city,
              province: payer_data.payer_information_guest_table.province,
              postal_code:
                payer_data.payer_information_guest_table.postal_code.toString(),
              phone: payer_data.payer_information_guest_table.phone.toString(),
            },
          });

          const createCart = await tx.CartTable.create({
            data: {
              payer_information_guest_table_id: +createPayer.id,
              discount_code: discount_code.toString(),
              status: true,
              status_guest: true,
              status_tax_invoice: tex_status,
            },
          });

          for (const data of cart_item) {
            await tx.CartItemTable.create({
              data: {
                ref_id: data.ref_id,
                qty: +data.qty,
                cart_table_id: +createCart.id,
                metadata: data.metadata,
              },
            });
          }

          if (tex_status === true) {
            await tx.TaxInformationTable.create({
              data: {
                cart_table_id: +createCart.id,
                name: tax_data[0].name,
                id_card: tax_data[0].id_card,
                address: tax_data[0].address,
                phone: tax_data[0].phone,
              },
            });
          }

          const createCartItem = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +createCart.id,
            },
            select: {
              qty: true,
              metadata: true,
            },
          });
          let sum = createCartItem.reduce((acc, product) => {
            return acc + product.qty * product.metadata.price;
          }, 0);

          const omise = require("omise")({
            secretKey: Constant.SECRET_KEY,
            omiseVersion: "2019-05-29",
          });
          let orderId =
            "order_" +
            moment().format("YYYY") +
            "_" +
            moment().format("MMDDHHmmssSSS") +
            "N";
          const createOrder = await tx.OrderTransactionTable.create({
            data: {
              order_id: orderId,
              transaction_id:
                "tran-padding-" +
                moment().format("YYYY") +
                "-" +
                moment().format("MMDDHHmmssSSS"),
              cart_table_id: +createCart.id,
              delivery_status: "...",
              status: "padding",
              // created_order_at: new Date(created_at),
              // paid_at: new Date(paid_at),
              total_price: parseFloat(sum.toString().replace(/,/g, "")),
              // paid_price: parseFloat(paid.toString().replace(/,/g, "")),
              // slip: [],
              charge_id:
                "charge-padding-" +
                moment().format("YYYY") +
                "-" +
                moment().format("MMDDHHmmssSSS"),
            },
          });

          const createCharges = await new Promise((resolve, reject) => {
            omise.charges.create(
              {
                description:
                  "Charge for customer :" +
                  payer_data.payer_information_guest_table.first_name +
                  " " +
                  payer_data.payer_information_guest_table.last_name,
                amount: 2000,
                currency: "thb",
                card: token,
              },
              async (err, resp) => {
                if (err) {
                  console.log("🚀 ~ returnnewPromise ~ err:", err);
                  return reject(err);
                }
                return resolve(resp);
              }
            );
          });

          let paid = +createCharges.amount / 100;

          const finishOrder = await tx.OrderTransactionTable.update({
            where: {
              id: +createOrder.id,
            },
            data: {
              transaction_id: createCharges.transaction,
              cart_table_id: +createCart.id,
              delivery_status: "...",
              status: createCharges.status,
              created_order_at: new Date(createCharges.created_at),
              paid_at: new Date(createCharges.paid_at),
              total_price: parseFloat(sum.toString().replace(/,/g, "")),
              paid_price: parseFloat(paid.toString().replace(/,/g, "")),
              // slip: [],
              charge_id: createCharges.id,
            },
          });

          const findCart = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +createCart.id,
            },
          });

          for (let itemCart of findCart) {
            let checkRef = itemCart.ref_id.slice(0, 1);

            if (checkRef === "P") {
              const findById = await tx.StockTable.findFirst({
                where: {
                  product_code: itemCart.ref_id,
                  color_name: itemCart.metadata.color_select.name,
                },
                select: {
                  id: true,
                  product_code: true,
                  product_name: true,
                  qty: true,
                  color_name: true,
                  status: true,
                },
              });

              await tx.StockMovementTable.create({
                data: {
                  stock_table_id: +findById.id,
                  status: 2,
                  qty: +itemCart.qty,
                  order_id: createOrder.order_id,
                },
              });

              await tx.StockTable.update({
                where: {
                  id: +findById.id,
                },
                data: {
                  qty:
                    +itemCart.qty >= +findById.qty
                      ? 0
                      : +findById.qty - +itemCart.qty,
                  status: +itemCart.qty >= +findById.qty ? 2 : 1,
                },
              });
            }
          }

          if (!_.isEmpty(finishOrder)) {
            const findCartItem = await tx.CartItemTable.findMany({
              where: {
                cart_table_id: +createCart.id,
              },
              select: {
                qty: true,
                metadata: true,
                ref_id: true,
              },
            });

            let createData = [];
            findCartItem.map((item, i) => {
              createData.push({
                numberno: i + 1,
                description:
                  item.ref_id.slice(0, 1) === "P"
                    ? item.metadata.product_name
                    : item.metadata.shape_name,
                quantity: item.qty,
                price: item.metadata.price,
              });
            });

            if (tex_status === true) {
              console.log(
                "🚀 ~ returnawaitprisma.$transaction ~ createData:",
                createData
              );

              fullTax(
                tax_data[0].name,
                tax_data[0].address,
                tax_data[0].id_card,
                tax_data[0].phone,
                orderId,
                moment().add(543, "years").format("YYYY-MM-DD"),
                createData,
                payer_data.payer_information_guest_table.email,
                h,
                prisma,
                +createOrder.id,
                1
              );
            } else {
              shortTax(
                tax_data.name,
                tax_data.address,
                tax_data.id_card,
                tax_data.phone,
                orderId,
                moment().add(543, "years").format("YYYY-MM-DD"),
                createData,
                payer_data.payer_information_guest_table.email,
                h,
                prisma,
                +createOrder.id,
                1
              );
            }

            return resolve({
              statusCode: 200,
              result: finishOrder,
            });
          }
        });
      };

      await transactionManageItemFromCart();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const finishCartGuessQrCode = (
  slip,
  payer_data,
  discount_code,
  tax_data,
  cart_item,
  tex_status,
  prisma
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionManageItemFromCart = async () => {
        return await prisma.$transaction(async (tx) => {
          const createPayer = await tx.PayerInformationGuestTable.create({
            data: {
              email: payer_data.payer_information_guest_table.email,
              country: payer_data.payer_information_guest_table.country,
              first_name: payer_data.payer_information_guest_table.first_name,
              last_name: payer_data.payer_information_guest_table.last_name,
              address: payer_data.payer_information_guest_table.address,
              optional: payer_data.payer_information_guest_table.optional,
              subdistrict: payer_data.payer_information_guest_table.subdistrict,
              city: payer_data.payer_information_guest_table.city,
              province: payer_data.payer_information_guest_table.province,
              postal_code:
                payer_data.payer_information_guest_table.postal_code.toString(),
              phone: payer_data.payer_information_guest_table.phone.toString(),
            },
          });

          const createCart = await tx.CartTable.create({
            data: {
              payer_information_guest_table_id: +createPayer.id,
              discount_code: discount_code.toString(),
              status: true,
              status_guest: true,
              status_tax_invoice: tex_status,
            },
          });

          for (const data of cart_item) {
            await tx.CartItemTable.create({
              data: {
                ref_id: data.ref_id,
                qty: +data.qty,
                cart_table_id: +createCart.id,
                metadata: data.metadata,
              },
            });
          }

          if (tex_status === true) {
            await tx.TaxInformationTable.create({
              data: {
                cart_table_id: +createCart.id,
                name: tax_data[0].name,
                id_card: tax_data[0].id_card,
                address: tax_data[0].address,
                phone: tax_data[0].phone,
              },
            });
          }

          const createCartItem = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +createCart.id,
            },
            select: {
              qty: true,
              metadata: true,
            },
          });
          let sum = createCartItem.reduce((acc, product) => {
            return acc + product.qty * product.metadata.price;
          }, 0);

          let orderId =
            "order_" +
            moment().format("YYYY") +
            "_" +
            moment().format("MMDDHHmmssSSS") +
            "N";
          const createOrder = await tx.OrderManualTransactionTable.create({
            data: {
              order_id: orderId,
              transaction_id:
                "tran-padding-" +
                moment().format("YYYY") +
                "-" +
                moment().format("MMDDHHmmssSSS"),
              cart_table_id: +createCart.id,
              delivery_status: "...",
              status: "padding",
              total_price: parseFloat(sum.toString().replace(/,/g, "")),
              created_order_at: new Date(),
              paid_at: new Date(),
              paid_price: parseFloat(sum.toString().replace(/,/g, "")),
              slip: [slip],
            },
          });

          if (!_.isEmpty(createOrder)) {
            return resolve({
              statusCode: 200,
              result: createOrder,
            });
          }
        });
      };

      await transactionManageItemFromCart();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const manualCreateOrder = (order, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionManualCreateOrder = async () => {
        return await prisma.$transaction(async (tx) => {
          console.log("🚀 ~ manualCreateOrder ~ order:", order);
          let createPayerInformationGuestTable = null;
          if (typeof order.cart_info.customer_table_id !== "object") {
            createPayerInformationGuestTable =
              await tx.PayerInformationGuestTable.create({
                data: {
                  email: order.payer_information_info.email,
                  country: order.payer_information_info.country,
                  first_name: order.payer_information_info.first_name,
                  last_name: order.payer_information_info.last_name,
                  address: order.payer_information_info.address,
                  optional: order.payer_information_info.optional,
                  subdistrict: order.payer_information_info.subdistrict,
                  city: order.payer_information_info.city,
                  province: order.payer_information_info.province,
                  postal_code: order.payer_information_info.postal_code,
                  phone: order.payer_information_info.phone,
                },
              });
          }
          const createCart = await tx.CartTable.create({
            data: {
              customer_table_id:
                typeof order.cart_info.customer_table_id === "object"
                  ? order.cart_info.customer_table_id.id
                  : null,
              user_table_id:
                typeof order.cart_info.user_table_id === "object"
                  ? order.cart_info.user_table_id.id
                  : null,
              payer_information_guest_table_id:
                typeof order.cart_info.customer_table_id === "object"
                  ? null
                  : createPayerInformationGuestTable.id,
              discount_code: order.cart_info.discount_code.toString(),
              status_user:
                typeof order.cart_info.customer_table_id === "object"
                  ? true
                  : false,
              status_guest:
                typeof order.cart_info.customer_table_id === "object"
                  ? false
                  : true,
              status_tax_invoice: order.cart_info.status_tax_invoice,
              status: false,
            },
          });

          if (createCart.status_tax_invoice === true) {
            await tx.TaxInformationTable.create({
              data: {
                cart_table_id: createCart.id,
                name: order.tax_information_info.name,
                id_card: order.tax_information_info.id_card,
                address: order.tax_information_info.address,
                phone: order.tax_information_info.phone,
              },
            });
          }

          if (order.product_item.length !== 0) {
            for (const item of order.product_item) {
              await tx.CartItemTable.create({
                data: {
                  ref_id: item.ref_id,
                  qty: +item.qty,
                  metadata: item.metadata,
                  cart_table_id: createCart.id,
                },
              });
            }
          }

          if (order.product_custom_item.length !== 0) {
            for (const item of order.product_custom_item) {
              await tx.CartItemTable.create({
                data: {
                  ref_id: item.ref_id,
                  qty: +item.qty,
                  metadata: item.metadata,
                  cart_table_id: createCart.id,
                },
              });
            }
          }

          let orderId =
            "order_" +
            moment().format("YYYY") +
            "_" +
            moment().format("MMDDHHmmssSSS") +
            "M";
          const createOrder = await tx.OrderManualTransactionTable.create({
            data: {
              order_id: orderId,
              transaction_id:
                "tran-padding-" +
                moment().format("YYYY") +
                "-" +
                moment().format("MMDDHHmmssSSS"),
              cart_table_id: +createCart.id,
              delivery_status: "...",
              status: "pending",
              total_price: parseFloat(order.sum_product.replace(/,/g, "")),
              slip: order.slip,
            },
          });

          if (!_.isEmpty(createOrder)) {
            return resolve({
              statusCode: 200,
              result: createOrder,
            });
          }
        });
      };

      await transactionManualCreateOrder();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const uploadSlipManualOrder = (order, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionUploadSlipManualOrder = async () => {
        return await prisma.$transaction(async (tx) => {
          const upload = await tx.OrderManualTransactionTable.update({
            where: {
              id: order.id,
            },
            data: {
              paid_price: +order.paid_price,
              created_order_at: new Date(),
              paid_at: new Date(),
              status: "paid",
              slip: order.slip,
            },
          });

          await tx.CartTable.update({
            where: {
              id: +upload.cart_table_id,
            },
            data: {
              status: true,
            },
          });

          if (!_.isEmpty(upload)) {
            return resolve({
              statusCode: 200,
              result: upload,
            });
          }
        });
      };

      await transactionUploadSlipManualOrder();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const confirmManualOrder = (order, prisma, h) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionConfirmManualOrder = async () => {
        return await prisma.$transaction(async (tx) => {
          const upload = await tx.OrderManualTransactionTable.update({
            where: {
              id: order.id,
            },
            data: {
              status: "successful",
            },
          });

          const findData = await tx.OrderManualTransactionTable.findFirst({
            where: {
              id: order.id,
            },
          });

          const findCart = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +findData.cart_table_id,
            },
          });

          for (let itemCart of findCart) {
            let checkRef = itemCart.ref_id.slice(0, 1);

            if (checkRef === "P") {
              const findById = await tx.StockTable.findFirst({
                where: {
                  product_code: itemCart.ref_id,
                  color_name: itemCart.metadata.color_select.name,
                },
                select: {
                  id: true,
                  product_code: true,
                  product_name: true,
                  qty: true,
                  color_name: true,
                  status: true,
                },
              });

              await tx.StockMovementTable.create({
                data: {
                  stock_table_id: +findById.id,
                  status: 2,
                  qty: +itemCart.qty,
                  order_id: findData.order_id,
                },
              });

              await tx.StockTable.update({
                where: {
                  id: +findById.id,
                },
                data: {
                  qty:
                    +itemCart.qty >= +findById.qty
                      ? 0
                      : +findById.qty - +itemCart.qty,
                  status: +itemCart.qty >= +findById.qty ? 2 : 1,
                },
              });
            }
          }

          const findCartItem = await tx.CartItemTable.findMany({
            where: {
              cart_table_id: +findData.cart_table_id,
            },
            select: {
              qty: true,
              metadata: true,
              ref_id: true,
            },
          });

          let createData = [];
          findCartItem.map((item, i) => {
            createData.push({
              numberno: i + 1,
              description:
                item.ref_id.slice(0, 1) === "P"
                  ? item.metadata.product_name
                  : item.metadata.shape_name,
              quantity: item.qty,
              price: item.metadata.price,
            });
          });

          const findCartTax = await tx.CartTable.findFirst({
            where: {
              id: +findData.cart_table_id,
            },
          });

          let email = "";
          if (findCartTax.status_guest === true) {
            const findEmail = await tx.PayerInformationGuestTable.findFirst({
              where: {
                id: +findCartTax.payer_information_guest_table_id,
              },
            });
            email = findEmail.email;
          } else {
            const findEmail = await tx.PayerInformationTable.findFirst({
              where: {
                customer_id: +findCartTax.customer_table_id,
              },
            });
            email = findEmail.email;

            const findCartFilter = await prisma.CartTable.findMany({
              orderBy: {
                created_at: "desc",
              },
              where: {
                customer_table_id: +findCartTax.customer_table_id,
              },
              select: {
                id: true,
                CartItemTable: {
                  select: {
                    id: true,
                    ref_id: true,
                    qty: true,
                    metadata: true,
                  },
                },
                OrderTransactionTable: {
                  select: {
                    id: true,
                    order_id: true,
                    charge_id: true,
                    transaction_id: true,
                    delivery_status: true,
                    status: true,
                    total_price: true,
                    paid_price: true,
                    created_order_at: true,
                    paid_at: true,
                  },
                },
                OrderManualTransactionTable: {
                  select: {
                    id: true,
                    order_id: true,
                    transaction_id: true,
                    delivery_status: true,
                    status: true,
                    total_price: true,
                    paid_price: true,
                    created_order_at: true,
                    paid_at: true,
                  },
                },
              },
            });

            let resultType1 = findCartFilter.filter(
              (item) => item.OrderTransactionTable.length !== 0
            );

            const findCartManual = await prisma.CartTable.findMany({
              orderBy: {
                created_at: "desc",
              },
              where: {
                customer_table_id: +findCartTax.customer_table_id,
              },
              select: {
                id: true,
                CartItemTable: {
                  select: {
                    id: true,
                    ref_id: true,
                    qty: true,
                    metadata: true,
                  },
                },
                OrderManualTransactionTable: {
                  select: {
                    id: true,
                    order_id: true,
                    transaction_id: true,
                    delivery_status: true,
                    status: true,
                    total_price: true,
                    paid_price: true,
                    created_order_at: true,
                    paid_at: true,
                  },
                },
              },
            });
            let resultType2 = findCartManual.filter(
              (item) => item.OrderManualTransactionTable.length !== 0
            );

            let sumNormal = resultType1.reduce((total, object) => {
              return total + object.OrderTransactionTable[0].total_price;
            }, 0);

            let sumQr = resultType2.reduce((total, object) => {
              return total + object.OrderManualTransactionTable[0].total_price;
            }, 0);

            let score = sumNormal + sumQr;

            const findLevel = await tx.levelTable.findFirst({
              where: {
                level_score: {
                  lte: score === 0 ? parseFloat(findData.total_price) : score,
                },
              },
              orderBy: {
                level_score: "desc",
              },
            });

            await tx.CustomerTable.update({
              where: {
                id: +findCartTax.customer_table_id,
              },
              data: {
                level_table_id: findLevel.id,
              },
            });
          }

          const findTax = await tx.TaxInformationTable.findFirst({
            where: {
              cart_table_id: +findData.cart_table_id,
            },
          });

          if (findCartTax.status_tax_invoice === true) {
            fullTax(
              findTax.name,
              findTax.address,
              findTax.id_card,
              findTax.phone,
              findData.order_id,
              moment().add(543, "years").format("YYYY-MM-DD"),
              createData,
              email,
              h,
              prisma,
              +order.id,
              2
            );
          } else {
            shortTax(
              "",
              "",
              "",
              "",
              findData.order_id,
              moment().add(543, "years").format("YYYY-MM-DD"),
              createData,
              email,
              h,
              prisma,
              +order.id,
              2
            );
          }

          if (!_.isEmpty(upload)) {
            return resolve({
              statusCode: 200,
              result: upload,
            });
          }
        });
      };

      await transactionConfirmManualOrder();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const addStock = (
  product_id,
  product_code,
  product_name,
  color_name,
  color,
  status,
  qty_movement,
  cost,
  id_user,
  prisma
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionStock = async () => {
        return await prisma.$transaction(async (tx) => {
          const addStock = await tx.StockTable.create({
            data: {
              product_table_id: product_id,
              product_code: product_code,
              product_name: product_name,
              color_name: color_name,
              color: color,
              qty: +qty_movement,
              status: qty_movement === 0 ? 2 : 1,
            },
          });

          const addStockMovement = await tx.StockMovementTable.create({
            data: {
              stock_table_id: addStock.id,
              status: status,
              qty: +qty_movement,
              cost: +cost,
              user_id: +id_user,
            },
          });

          const getStock = await tx.StockTable.findMany({
            orderBy: {
              created_at: "desc",
            },
            select: {
              id: true,
              product_code: true,
              product_name: true,
              qty: true,
              color_name: true,
              status: true,
              StockMovementTable: {
                orderBy: {
                  created_at: "desc",
                },
                where: {
                  OR: [
                    {
                      status: 1,
                    },
                    {
                      status: 3,
                    },
                  ],
                },
                select: {
                  id: true,
                  qty: true,
                  cost: true,
                  change_qty: true,
                  change_cost: true,
                  status: true,
                },
                take: 1,
              },
            },
          });

          if (!_.isEmpty(addStock) && !_.isEmpty(addStockMovement)) {
            return resolve({
              statusCode: 200,
              result: getStock,
            });
          }
        });
      };

      await transactionStock();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const editStock = (stock_id, status, qty_movement, cost, id_user, prisma) => {
  return new Promise(async (resolve, reject) => {
    try {
      const transactionStock = async () => {
        return await prisma.$transaction(async (tx) => {
          const editStockMovement = await tx.StockMovementTable.create({
            data: {
              stock_table_id: +stock_id,
              status: +status,
              qty: +status === 1 ? +qty_movement : null,
              cost: +status === 1 ? +cost : null,
              change_qty: +status === 3 ? +qty_movement : null,
              change_cost: +status === 3 ? +cost : null,
              user_id: +id_user,
            },
          });

          const findById = await tx.StockTable.findFirst({
            where: {
              id: +stock_id,
            },
            select: {
              id: true,
              product_code: true,
              product_name: true,
              qty: true,
              color_name: true,
              status: true,
            },
          });

          const editStock = await tx.StockTable.update({
            where: {
              id: +stock_id,
            },
            data: {
              qty: +status === 1 ? +qty_movement + findById.qty : +qty_movement,
              status: +status === 1 ? 1 : +qty_movement > 0 ? 1 : 0,
            },
          });

          const getStock = await tx.StockTable.findMany({
            orderBy: {
              created_at: "desc",
            },
            select: {
              id: true,
              product_code: true,
              product_name: true,
              qty: true,
              color_name: true,
              status: true,
              StockMovementTable: {
                orderBy: {
                  created_at: "desc",
                },
                where: {
                  OR: [
                    {
                      status: 1,
                    },
                    {
                      status: 3,
                    },
                  ],
                },
                select: {
                  id: true,
                  qty: true,
                  cost: true,
                  change_qty: true,
                  change_cost: true,
                  status: true,
                },
                take: 1,
              },
            },
          });

          if (!_.isEmpty(editStock) && !_.isEmpty(editStockMovement)) {
            return resolve({
              statusCode: 200,
              result: getStock,
            });
          }
        });
      };

      await transactionStock();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const fullTax = (
  name,
  address,
  tax_id,
  phone,
  number_order,
  date_order,
  item,
  email,
  h,
  prisma,
  order_id,
  typepay
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fonts = {
        Kanit: {
          italics: "example/fonts/Kanit-Light.ttf",
          normal: "example/fonts/Kanit-Regular.ttf",
          bold: "example/fonts/Kanit-Bold.ttf",
          // italics: "example/fonts/Kanit-Italic.ttf",
          bolditalics: "example/fonts/Kanit-BoldItalic.ttf",
        },
      };

      const invoiceData = {
        customer: {
          name: name,
          address: address,
          taxId: tax_id,
          phone: phone,
          email: email,
        },
        invoice: {
          number: number_order,
          date: date_order,
          items: item,
        },
      };

      const calculateTotal = (items) => {
        let sumtotal = items.reduce(
          (sum, item) => sum + item.quantity * item.price,
          0
        );
        let tax = sumtotal * 0.07;
        let subtotal = sumtotal - tax;
        let total = subtotal + tax;
        tax = tax.toFixed(2);
        subtotal = subtotal.toFixed(2);
        total = total.toFixed(2);
        return { subtotal, tax, total };
      };

      const priceBeforeTax = (items) => {
        let tax = items * 0.07;
        let subtotal = items - tax;
        subtotal = subtotal.toFixed(2);
        return subtotal;
      };

      const toBath = (items) => {
        let bath = bahttext(items);
        wordcut.init();
        let warpBath = wordcut.cut(bath);
        const fontSize = 9;
        const maxLineLength = 250;

        let output = "";
        let currentLineLength = 0;

        const words = warpBath.split("|");

        words.forEach((word, index) => {
          const wordLength = word.length * fontSize;
          if (currentLineLength + wordLength > maxLineLength) {
            output += "\n";
            currentLineLength = 0;
          }

          output += word;
          currentLineLength += wordLength;

          if (index !== words.length - 1) {
            output += "";
            currentLineLength += fontSize;
          }
        });

        return output;
      };

      const { subtotal, tax, total } = calculateTotal(
        invoiceData.invoice.items
      );

      // const docDefinition = {
      //   content: [
      //     {
      //       columns: [
      //         [{ text: "ใบกำกับภาษี", style: "header" }],
      //         [
      //           {
      //             text: `เลขที่: ${invoiceData.invoice.number}`,
      //             alignment: "right",
      //           },
      //           {
      //             text: `วันที่: ${invoiceData.invoice.date}`,
      //             alignment: "right",
      //           },
      //         ],
      //       ],
      //     },
      //     { text: "\n" },
      //     {
      //       columns: [
      //         [
      //           { text: "ข้อมูลผู้ขาย", style: "subheader" },
      //           { text: invoiceData.company.name },
      //           { text: invoiceData.company.address },
      //           { text: `โทร: ${invoiceData.company.phone}` },
      //           {
      //             text: `เลขประจำตัวผู้เสียภาษี: ${invoiceData.company.taxId}`,
      //           },
      //         ],
      //         [
      //           { text: "ข้อมูลผู้ซื้อ", style: "subheadertwo" },
      //           { text: invoiceData.customer.name },
      //           { text: invoiceData.customer.address },
      //           {
      //             text: `เลขประจำตัวผู้เสียภาษี: ${invoiceData.customer.taxId}`,
      //           },
      //           { text: `โทร: ${invoiceData.customer.phone}` },
      //         ],
      //       ],
      //     },
      //     { text: "\n" },
      //     {
      //       table: {
      //         headerRows: 1,
      //         widths: ["auto", "*", "auto", "auto", "auto"],
      //         body: [
      //           [
      //             { text: "ลำดับ", style: "tableHeader" },
      //             { text: "รายละเอียดสินค้า", style: "tableHeader" },
      //             { text: "จำนวน", style: "tableHeader" },
      //             { text: "ราคาต่อหน่วย", style: "tableHeader" },
      //             { text: "รวม", style: "tableHeader" },
      //           ],
      //           ...invoiceData.invoice.items.map((item) => [
      //             item.numberno,
      //             item.description,
      //             item.quantity,
      //             item.price.toFixed(2),
      //             (item.quantity * item.price).toFixed(2),
      //           ]),
      //           [
      //             { text: "รวม", colSpan: 4, alignment: "right" },
      //             {},
      //             {},
      //             {},
      //             subtotal.toFixed(2),
      //           ],
      //           [
      //             {
      //               text: "ภาษีมูลค่าเพิ่ม (7%)",
      //               colSpan: 4,
      //               alignment: "right",
      //             },
      //             {},
      //             {},
      //             {},
      //             tax.toFixed(2),
      //           ],
      //           [
      //             { text: "รวมทั้งหมด", colSpan: 4, alignment: "right" },
      //             {},
      //             {},
      //             {},
      //             total.toFixed(2),
      //           ],
      //         ],
      //       },
      //     },
      //   ],
      //   styles: {
      //     header: {
      //       fontSize: 18,
      //       bold: true,
      //       margin: [0, 0, 0, 10],
      //     },
      //     subheader: {
      //       fontSize: 14,
      //       bold: true,
      //       margin: [0, 0, 0, 5],
      //     },
      //     subheadertwo: {
      //       fontSize: 14,
      //       bold: true,
      //       margin: [0, 0, 0, 5],
      //     },
      //     tableHeader: {
      //       bold: true,
      //       fillColor: "white",
      //       color: "black",
      //       margin: [8, 8, 8, 8],
      //     },
      //   },
      //   defaultStyle: {
      //     font: "THSarabunNew",
      //   },
      //   footer: function (currentPage, pageCount) {
      //     return {
      //       text: currentPage.toString() + " / " + pageCount,
      //       alignment: "center",
      //       fontSize: 12,
      //       margin: [0, 0, 0, 20],
      //     };
      //   },
      // };

      // const pdfDoc = printer.createPdfKitDocument(docDefinition);
      // let chunks = [];
      // pdfDoc.on("data", (chunk) => {
      //   chunks.push(chunk);
      // });
      // pdfDoc.on("end", async () => {
      //   const pdfBuffer = Buffer.concat(chunks);

      //   let transporter = nodemailer.createTransport({
      //     service: "Gmail",
      //     host: "smtp.gmail.com",
      //     port: 465,
      //     secure: true,
      //     auth: {
      //       user: "chindanai.billy@gmail.com",
      //       pass: Constant.PASSEMAIL,
      //     },
      //   });

      //   let mailOptions = {
      //     from: "chindanai.billy@gmail.com",
      //     to: email,
      //     subject: "This is Tax",
      //     text: "Tax",
      //     // html: "<b>Hello world?</b>",
      //     attachments: [
      //       {
      //         filename: "tax" + moment().format("YYYY_MM_DDTHH_mm_ss") + ".pdf",
      //         content: pdfBuffer,
      //       },
      //     ],
      //   };

      //   try {
      //     let info = await transporter.sendMail(mailOptions);
      //     console.log("Message sent: %s", info);
      //   } catch (error) {
      //     console.error(error);
      //   }
      // });

      // pdfDoc.end();

      const printer = new PdfPrinter(fonts);

      let createContent = [
        {
          style: "tableExample",
          layout: {
            defaultBorder: false,
          },
          table: {
            dontBreakRows: true,
            widths: ["*"],
            body: [
              [
                {
                  columns: [
                    {
                      headlineLevel: 1,
                      margin: [0, 0, 0, 0],
                      canvas: [
                        {
                          type: "rect",
                          x: 0,
                          y: 0,
                          w: 514,
                          h: 1,
                          color: "#F2F2F2",
                        },
                      ],
                    },
                  ],
                },
              ],
              [
                {
                  style: "tableExample",
                  layout: {
                    defaultBorder: false,
                  },
                  table: {
                    headlineLevel: 1,
                    widths: [50, 132, 90, 98, 100],
                    body: [
                      [
                        {
                          columns: [
                            {
                              image: "assets/list-pdf.png",
                              width: 6,
                              margin: [0, 2, 0, 0],
                            },
                            {
                              text: "สรุป",
                              style: "headercolone",
                              margin: [2, 0, 0, 0],
                            },
                          ],
                        },
                        { text: "มูลค่ารายการภาษีมูลค่าเพิ่มอัตรา 7%" },
                        {
                          text: `${commaNumber(subtotal)} บาท`,
                          alignment: "right",
                          italics: true,
                        },
                        { text: "", fillColor: "#F2F2F2" },
                        { text: "", fillColor: "#F2F2F2" },
                      ],
                      [
                        {},
                        { text: "ภาษีมูลค่าเพิ่มรวม" },
                        {
                          text: `${commaNumber(tax)} บาท`,
                          alignment: "right",
                          italics: true,
                        },
                        {
                          text: "จำนวนเงินทั้งสิ้น",
                          fillColor: "#F2F2F2",
                          alignment: "left",
                          italics: true,
                        },
                        {
                          fillColor: "#F2F2F2",
                          columns: [
                            {
                              text: commaNumber(total),
                              alignment: "right",
                              fontSize: 12,
                              margin: [0, -3, 2, 0],
                            },
                            { text: "บาท" },
                          ],
                        },
                      ],
                      [
                        {},
                        { text: "จำนวนเงินทั้งสิ้น" },
                        {
                          text: toBath(total),
                          // alignment: "right",
                          italics: true,
                        },
                        {
                          text: "",
                          fillColor: "#F2F2F2",
                        },
                        { text: "", fillColor: "#F2F2F2" },
                      ],
                      [
                        {},
                        {},
                        {},
                        {
                          text: "จำนวนเงินที่ถูกหัก ณ ที่จ่าย",
                          alignment: "right",
                          margin: [0, 7, 0, 0],
                        },
                        {
                          text: `${commaNumber(total)} บาท`,
                          italics: true,
                          alignment: "right",
                          margin: [0, 7, 0, 0],
                        },
                      ],
                      [
                        {},
                        {},
                        {},
                        {
                          text: "จำนวนเงินที่ชำระ",
                          alignment: "right",
                        },
                        {
                          text: `${commaNumber(total)} บาท`,
                          italics: true,
                          alignment: "right",
                        },
                      ],
                    ],
                  },
                },
              ],
              [
                {
                  columns: [
                    {
                      canvas: [
                        {
                          type: "rect",
                          x: 0,
                          y: 0,
                          w: 514,
                          h: 1,
                          color: "#F2F2F2",
                        },
                      ],
                    },
                  ],
                },
              ],
              [
                {
                  style: "tableExample",
                  layout: {
                    defaultBorder: false,
                  },
                  table: {
                    widths: [50, 60, 60, 200],
                    body: [
                      [
                        {
                          columns: [
                            {
                              image: "assets/money-pdf.png",
                              width: 10,
                              margin: [0, 3, 0, 0],
                            },
                            {
                              text: "ชำระเงิน",
                              style: "headercolone",
                              margin: [2, 0, 0, 0],
                            },
                          ],
                        },
                        { text: "วันที่ชำระ :" },
                        { text: invoiceData.invoice.date, alignment: "right" },
                        {
                          margin: [30, 0, 0, 0],
                          columns: [
                            {
                              image: "assets/money-pdf.png",
                              width: 10,
                              margin: [0, 3, 0, 0],
                            },
                            {
                              text: "เงินสด เงินสด",
                              style: "headercolone",
                              italics: true,
                              margin: [10, 0, 0, 0],
                            },
                          ],
                        },
                      ],
                      [
                        {},
                        { text: "จำนวนเงินรวม :" },
                        {
                          text: `${commaNumber(total)} บาท`,
                          alignment: "right",
                        },
                        {},
                      ],
                    ],
                  },
                },
              ],
              [
                {
                  columns: [
                    {
                      canvas: [
                        {
                          type: "rect",
                          x: 0,
                          y: 0,
                          w: 514,
                          h: 1,
                          color: "#F2F2F2",
                        },
                      ],
                    },
                  ],
                },
              ],
              [
                {
                  style: "tableExample",
                  layout: {
                    defaultBorder: false,
                  },
                  table: {
                    widths: [50, "*"],
                    body: [
                      [
                        {
                          columns: [
                            {
                              image: "assets/comment-pdf.png",
                              width: 10,
                              margin: [0, 3, 0, 0],
                            },
                            {
                              text: "หมายเหตุ",
                              style: "headercolone",
                              margin: [2, 0, 0, 0],
                            },
                          ],
                        },
                        { text: "", italics: true },
                      ],
                    ],
                  },
                },
              ],
              [
                {
                  columns: [
                    {
                      canvas: [
                        {
                          type: "rect",
                          x: 0,
                          y: 0,
                          w: 514,
                          h: 1,
                          color: "#F2F2F2",
                        },
                      ],
                    },
                  ],
                },
              ],
              [
                {
                  style: "tableExample",
                  layout: {
                    defaultBorder: false,
                  },
                  table: {
                    // dontBreakRows: true,
                    widths: [50, "*", "*", "*", "*"],
                    body: [
                      [
                        {
                          columns: [
                            {
                              image: "assets/signature-pdf.png",
                              width: 10,
                              margin: [0, 3, 0, 0],
                            },
                            {
                              text: "รับรอง",
                              style: "headercolone",
                              margin: [2, 0, 0, 0],
                            },
                          ],
                        },
                        {
                          text: "ผู้จัดทำเอกสาร (ผู้ขาย)",
                          alignment: "center",
                        },
                        {
                          text: "ผู้อนุมัติเอกสาร (ผู้ขาย)",
                          alignment: "center",
                        },
                        {
                          text: "ตราประทับ (ผู้ขาย)",
                          alignment: "center",
                        },
                        {
                          text: "ผู้รับเอกสาร (ลูกค้า)",
                          alignment: "center",
                        },
                      ],
                      [
                        {},
                        {
                          stack: [
                            {
                              text: "",
                              margin: [0, 70, 0, 0],
                            },
                            {
                              text: "กัญญาณ์ ปาริษวุฒิ",
                              alignment: "center",
                              fontSize: 11,
                            },
                            {
                              text: invoiceData.invoice.date,
                              alignment: "center",
                              style: "headercolone",
                              italics: true,
                            },
                          ],
                        },
                        {
                          margin: [15, 10, 0, 0],
                          stack: [
                            {
                              image: "assets/signature-jira.png",
                              width: 80,
                            },
                            {
                              text: "Pudisorn Chatvorapat",
                              alignment: "center",
                              fontSize: 11,
                              margin: [0, 30, 0, 0],
                            },
                            {
                              text: invoiceData.invoice.date,
                              alignment: "center",
                              style: "headercolone",
                              italics: true,
                            },
                          ],
                        },
                        {
                          margin: [15, 0, 0, 0],
                          stack: [
                            {
                              image: "assets/logo-perm-suk-sap-color.png",
                              width: 80,
                            },
                          ],
                        },
                        {},
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
      ];

      const docDefinition = {
        content: [
          {
            absolutePosition: { x: 410, y: 92 },
            canvas: [
              {
                type: "rect",
                x: 0,
                y: 0,
                w: 160,
                h: 70,
                color: "#F2F2F2",
              },
            ],
          },
          {
            columns: [
              {
                stack: [
                  {
                    image: "assets/logo-perm-suk-sap.png",
                    width: 40,
                    alignment: "left",
                  },
                ],
              },
              {
                stack: [
                  {
                    text: "(ต้นฉบับ)",
                    style: "typeinvoie",
                    alignment: "right",
                  },
                  {
                    text: "ใบเสร็จรับเงิน/ใบกำกับภาษี",
                    style: "textinvoie",
                    noWrap: true,
                  },
                ],
              },
            ],
          },
          {
            columns: [
              {
                margin: [0, 13, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        width: 45,
                        text: "ผู้ขาย :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text: "บริษัท เพิ่มพรฟูรณ์ จำกัด",
                        style: "headercolone",
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 45,
                        text: "ที่อยู่ :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text:
                          "เลขที่ 14 ซอย ประดิษฐ์มนูธรรม 27 แยก 8 แขวง\n" +
                          "ราชบุรณะ  บางบอน\n" +
                          "กรุงเทพมหานคร 10140",
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 45,
                        text: "เลขที่ภาษี :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text: "0105546044659 (สำนักงานใหญ่)",
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                ],
              },
              {
                margin: [17, 13, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        image: "assets/phone-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "0916159566",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        image: "assets/email-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "jirastudio.jira@gmail.com",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        image: "assets/host-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "https://jirastudioshop.com/",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                ],
              },
              {
                margin: [-45, 13, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        width: 60,
                        text: "เลขที่เอกสาร :",
                        style: "headercolone",
                      },
                      {
                        width: 90,
                        text: invoiceData.invoice.number,
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 60,
                        text: "วันที่ออก :",
                        style: "headercolone",
                      },
                      {
                        width: 120,
                        text: invoiceData.invoice.date,
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 60,
                        text: "อ้างอิง :",
                        style: "headercolone",
                      },
                      {
                        width: 120,
                        text: "-",
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            columns: [
              {
                margin: [1, 10, 0, 0],
                canvas: [
                  {
                    type: "rect",
                    x: 0,
                    y: 0,
                    w: 365,
                    h: 1,
                    color: "#F2F2F2",
                  },
                ],
              },
              {
                columns: [
                  {
                    width: 80,
                    text: "ติดต่อกลับที่ :",
                    style: "headercolone",
                    margin: [10, 0, 0, 0],
                  },
                ],
              },
            ],
          },
          {
            columns: [
              {
                margin: [0, 5, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        width: 45,
                        text: "ลูกค้า :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text: invoiceData.customer.name,
                        style: "headercolone",
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 45,
                        text: "ที่อยู่ :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text: invoiceData.customer.address,
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 45,
                        text: "เลขที่ภาษี :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text: invoiceData.customer.taxId,
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                ],
              },
              {
                margin: [17, 5, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        image: "assets/phone-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: invoiceData.customer.phone,
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        image: "assets/email-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: invoiceData.customer.email,
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        image: "assets/host-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "-",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                ],
              },
              {
                margin: [-45, 5, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        image: "assets/phone-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "0909284453",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        image: "assets/email-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "tarjira599@gmail.com",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            style: "tableExample",
            layout: {
              defaultBorder: false,
            },
            table: {
              headerRows: 1,
              widths: [14, "*", 40, 60, 40, 20, 60],
              body: [
                [
                  { text: "คำอธิบาย", style: "tableHeader", colSpan: 2 },
                  {},
                  { text: "จำนวน", style: "tableHeader", alignment: "right" },
                  { text: "ราคา", style: "tableHeader", alignment: "right" },
                  { text: "ส่วนลด", style: "tableHeader", alignment: "right" },
                  { text: "VAT", style: "tableHeader", alignment: "right" },
                  {
                    text: "มูลค่าก่อนภาษี",
                    style: "tableHeader",
                    alignment: "right",
                  },
                ],
                ...invoiceData.invoice.items.map((item) => [
                  { text: item.numberno + ".", style: "tbody" },
                  {
                    text: `${item.description} : ${item.description}`,
                    italics: true,
                  },
                  {
                    text: item.quantity.toFixed(2),
                    alignment: "right",
                    style: "tbody",
                  },
                  {
                    text: commaNumber((item.price * item.quantity).toFixed(2)),
                    alignment: "right",
                    style: "tbody",
                  },
                  {
                    text: commaNumber((0).toFixed(2)),
                    alignment: "right",
                    style: "tbody",
                  },
                  { text: "7%", alignment: "right", style: "tbody" },
                  {
                    text: commaNumber(
                      priceBeforeTax(item.price * item.quantity)
                    ),
                    alignment: "right",
                    style: "tbody",
                  },
                ]),
              ],
            },
          },
          ...createContent,
        ],
        styles: {
          header: {
            fontSize: 14,
            bold: true,
            margin: [0, 10, 0, 10],
          },
          headercolone: {
            fontSize: 9,
          },
          tbody: {
            margin: [0, 3, 0, 3],
            italics: true,
          },
          subheader: {
            fontSize: 12,
            margin: [0, 5, 0, 5],
          },
          typeinvoie: {
            fontSize: 9,
            margin: [0, 0, 0, -5],
          },
          textinvoie: {
            fontSize: 26,
            margin: [-22, 0, 0, 0],
            color: "#848484",
          },
          tableHeader: {
            margin: [0, 5, 0, 5],
            fontSize: 10,
            color: "black",
            fillColor: "#F2F2F2",
          },
          tableExample: {
            fontSize: 9,
            margin: [0, 10, 0, 15],
          },
        },
        defaultStyle: {
          font: "Kanit",
        },
      };

      const pdfDoc = printer.createPdfKitDocument(docDefinition);
      let chunks = [];
      pdfDoc.on("data", (chunk) => {
        chunks.push(chunk);
      });
      pdfDoc.on("end", async () => {
        const thisBuffer = Buffer.concat(chunks);

        let pdfBuffer = h.response(thisBuffer).type("application/pdf");
        const uploadParams = {
          Bucket: "share",
          Key:
            "invoice_" +
            moment().format("DDMMYYYYSSS") +
            Math.random().toString(36).substring(2, 7) +
            "." +
            "pdf",
          Body: pdfBuffer.source,
          ContentType: pdfBuffer.headers["content-type"],
        };

        const upload = new Upload({
          client: S3,
          params: uploadParams,
        });

        const result = await upload.done();
        console.log("🚀 ~ pdfDoc.on ~ result:", result);
        if (typepay === 1) {
          await prisma.OrderTransactionTable.update({
            where: {
              id: +order_id,
            },
            data: {
              pdf_file: `https://${Constant.PATH_FILE}/${result.Key}`,
            },
          });
        } else {
          await prisma.OrderManualTransactionTable.update({
            where: {
              id: +order_id,
            },
            data: {
              pdf_file: `https://${Constant.PATH_FILE}/${result.Key}`,
            },
          });
        }
        let transporter = nodemailer.createTransport({
          service: "Gmail",
          host: "smtp.gmail.com",
          port: 465,
          secure: true,
          auth: {
            user: "chindanai.billy@gmail.com",
            pass: Constant.PASSEMAIL,
          },
        });

        let createSubject =
          "invoice_" +
          moment().format("DDMMYYYYSSS") +
          Math.random().toString(36).substring(2, 7);

        let createFilename =
          "invoice_" +
          moment().format("DDMMYYYYSSS") +
          Math.random().toString(36).substring(2, 7) +
          ".pdf";

        let mailOptions = {
          from: "chindanai.billy@gmail.com",
          to: email,
          subject: createSubject,
          text: "",
          attachments: [
            {
              filename: createFilename,
              content: thisBuffer,
            },
          ],
        };

        let mailOptionsForOwner = {
          from: "chindanai.billy@gmail.com",
          to: Constant.OWNER_EMAIL,
          subject: createSubject,
          text: "",
          attachments: [
            {
              filename: createFilename,
              content: thisBuffer,
            },
          ],
        };

        try {
          let info = await transporter.sendMail(mailOptions);
          let infoOwner = await transporter.sendMail(mailOptionsForOwner);
          console.log("Message sent: %s", info);
          console.log("Message sent: %s", infoOwner);
        } catch (error) {
          console.error(error);
        }
      });

      resolve({
        statusCode: 200,
      });
      pdfDoc.end();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

const shortTax = (
  name,
  address,
  tax_id,
  phone,
  number_order,
  date_order,
  item,
  email,
  h,
  prisma,
  order_id,
  typepay
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const fonts = {
        Kanit: {
          italics: "example/fonts/Kanit-Light.ttf",
          normal: "example/fonts/Kanit-Regular.ttf",
          bold: "example/fonts/Kanit-Bold.ttf",
          // italics: "example/fonts/Kanit-Italic.ttf",
          bolditalics: "example/fonts/Kanit-BoldItalic.ttf",
        },
      };

      const invoiceData = {
        customer: {
          name: name,
          address: address,
          taxId: tax_id,
          phone: phone,
        },
        invoice: {
          number: number_order,
          date: date_order,
          items: item,
        },
      };

      const calculateTotal = (items) => {
        let sumtotal = items.reduce(
          (sum, item) => sum + item.quantity * item.price,
          0
        );
        let tax = sumtotal * 0.07;
        let subtotal = sumtotal - tax;
        let total = subtotal + tax;
        tax = tax.toFixed(2);
        subtotal = subtotal.toFixed(2);
        total = total.toFixed(2);
        return { subtotal, tax, total };
      };

      const priceBeforeTax = (items) => {
        let tax = items * 0.07;
        let subtotal = items - tax;
        subtotal = subtotal.toFixed(2);
        return subtotal;
      };

      const toBath = (items) => {
        let bath = bahttext(items);
        wordcut.init();
        let warpBath = wordcut.cut(bath);
        const fontSize = 9;
        const maxLineLength = 250;

        let output = "";
        let currentLineLength = 0;

        const words = warpBath.split("|");

        words.forEach((word, index) => {
          const wordLength = word.length * fontSize;
          if (currentLineLength + wordLength > maxLineLength) {
            output += "\n";
            currentLineLength = 0;
          }

          output += word;
          currentLineLength += wordLength;

          if (index !== words.length - 1) {
            output += "";
            currentLineLength += fontSize;
          }
        });

        return output;
      };

      const { subtotal, tax, total } = calculateTotal(
        invoiceData.invoice.items
      );

      // const printer = new PdfPrinter(fonts);

      // const invoiceData = {
      //   company: {
      //     name: "Jira studio",
      //     address: "123 ถนนของคุณ, เมืองของคุณ, ประเทศของคุณ",
      //     phone: "012-345-6789",
      //     taxId: "1234567890",
      //   },
      //   customer: {
      //     name: name,
      //     address: address,
      //     taxId: tax_id,
      //     phone: phone,
      //   },
      //   invoice: {
      //     number: number_order,
      //     date: date_order,
      //     items: item,
      //   },
      // };

      // const calculateTotal = (items) => {
      //   let sumtotal = items.reduce(
      //     (sum, item) => sum + item.quantity * item.price,
      //     0
      //   );
      //   let tax = sumtotal * 0.07;
      //   let subtotal = sumtotal - tax;
      //   let total = subtotal + tax;
      //   return { subtotal, tax, total };
      // };

      // const { subtotal, tax, total } = calculateTotal(
      //   invoiceData.invoice.items
      // );

      // const docDefinition = {
      //   content: [
      //     {
      //       columns: [
      //         [{ text: "ใบกำกับภาษี", style: "header" }],
      //         [
      //           {
      //             text: `เลขที่: ${invoiceData.invoice.number}`,
      //             alignment: "right",
      //           },
      //           {
      //             text: `วันที่: ${invoiceData.invoice.date}`,
      //             alignment: "right",
      //           },
      //         ],
      //       ],
      //     },
      //     { text: "\n" },
      //     {
      //       columns: [
      //         [
      //           { text: "ข้อมูลผู้ขาย", style: "subheader" },
      //           { text: invoiceData.company.name },
      //           { text: invoiceData.company.address },
      //           { text: `โทร: ${invoiceData.company.phone}` },
      //           {
      //             text: `เลขประจำตัวผู้เสียภาษี: ${invoiceData.company.taxId}`,
      //           },
      //         ],
      //       ],
      //     },
      //     { text: "\n" },
      //     {
      //       table: {
      //         headerRows: 1,
      //         widths: ["*", "auto", "auto", "auto"],
      //         body: [
      //           [
      //             { text: "รายละเอียดสินค้า", style: "tableHeader" },
      //             { text: "จำนวน", style: "tableHeader" },
      //             { text: "ราคาต่อหน่วย", style: "tableHeader" },
      //             { text: "รวม", style: "tableHeader" },
      //           ],
      //           ...invoiceData.invoice.items.map((item) => [
      //             item.description,
      //             item.quantity,
      //             item.price.toFixed(2),
      //             (item.quantity * item.price).toFixed(2),
      //           ]),
      //           [
      //             { text: "รวม", colSpan: 3, alignment: "right" },
      //             {},
      //             {},
      //             subtotal.toFixed(2),
      //           ],
      //           [
      //             {
      //               text: "ภาษีมูลค่าเพิ่ม (7%)",
      //               colSpan: 3,
      //               alignment: "right",
      //             },
      //             {},
      //             {},
      //             tax.toFixed(2),
      //           ],
      //           [
      //             { text: "รวมทั้งหมด", colSpan: 3, alignment: "right" },
      //             {},
      //             {},
      //             total.toFixed(2),
      //           ],
      //         ],
      //       },
      //     },
      //   ],
      //   styles: {
      //     header: {
      //       fontSize: 18,
      //       bold: true,
      //       margin: [0, 0, 0, 10],
      //     },
      //     subheader: {
      //       fontSize: 14,
      //       bold: true,
      //       margin: [0, 0, 0, 5],
      //     },
      //     tableHeader: {
      //       bold: true,
      //       fillColor: "white",
      //       color: "black",
      //       margin: [8, 8, 8, 8],
      //     },
      //   },
      //   defaultStyle: {
      //     font: "THSarabunNew",
      //   },
      //   footer: function (currentPage, pageCount) {
      //     return {
      //       text: currentPage.toString() + " / " + pageCount,
      //       alignment: "center",
      //       fontSize: 12,
      //       margin: [0, 0, 0, 20],
      //     };
      //   },
      // };

      // pdfDoc.pipe(fs.createWriteStream("short_invoice.pdf"));
      // pdfDoc.end();

      const printer = new PdfPrinter(fonts);

      let createContent = [
        {
          style: "tableExample",
          layout: {
            defaultBorder: false,
          },
          table: {
            dontBreakRows: true,
            widths: ["*"],
            body: [
              [
                {
                  columns: [
                    {
                      headlineLevel: 1,
                      margin: [0, 0, 0, 0],
                      canvas: [
                        {
                          type: "rect",
                          x: 0,
                          y: 0,
                          w: 514,
                          h: 1,
                          color: "#F2F2F2",
                        },
                      ],
                    },
                  ],
                },
              ],
              [
                {
                  style: "tableExample",
                  layout: {
                    defaultBorder: false,
                  },
                  table: {
                    headlineLevel: 1,
                    widths: [50, 132, 90, 98, 100],
                    body: [
                      [
                        {
                          columns: [
                            {
                              image: "assets/list-pdf.png",
                              width: 6,
                              margin: [0, 2, 0, 0],
                            },
                            {
                              text: "สรุป",
                              style: "headercolone",
                              margin: [2, 0, 0, 0],
                            },
                          ],
                        },
                        { text: "มูลค่ารายการภาษีมูลค่าเพิ่มอัตรา 7%" },
                        {
                          text: `${commaNumber(subtotal)} บาท`,
                          alignment: "right",
                          italics: true,
                        },
                        { text: "", fillColor: "#F2F2F2" },
                        { text: "", fillColor: "#F2F2F2" },
                      ],
                      [
                        {},
                        { text: "ภาษีมูลค่าเพิ่มรวม" },
                        {
                          text: `${commaNumber(tax)} บาท`,
                          alignment: "right",
                          italics: true,
                        },
                        {
                          text: "จำนวนเงินทั้งสิ้น",
                          fillColor: "#F2F2F2",
                          alignment: "left",
                          italics: true,
                        },
                        {
                          fillColor: "#F2F2F2",
                          columns: [
                            {
                              text: commaNumber(total),
                              alignment: "right",
                              fontSize: 12,
                              margin: [0, -3, 2, 0],
                            },
                            { text: "บาท" },
                          ],
                        },
                      ],
                      [
                        {},
                        { text: "จำนวนเงินทั้งสิ้น" },
                        {
                          text: toBath(total),
                          // alignment: "right",
                          italics: true,
                        },
                        {
                          text: "",
                          fillColor: "#F2F2F2",
                        },
                        { text: "", fillColor: "#F2F2F2" },
                      ],
                      [
                        {},
                        {},
                        {},
                        {
                          text: "จำนวนเงินที่ถูกหัก ณ ที่จ่าย",
                          alignment: "right",
                          margin: [0, 7, 0, 0],
                        },
                        {
                          text: `${commaNumber(total)} บาท`,
                          italics: true,
                          alignment: "right",
                          margin: [0, 7, 0, 0],
                        },
                      ],
                      [
                        {},
                        {},
                        {},
                        {
                          text: "จำนวนเงินที่ชำระ",
                          alignment: "right",
                        },
                        {
                          text: `${commaNumber(total)} บาท`,
                          italics: true,
                          alignment: "right",
                        },
                      ],
                    ],
                  },
                },
              ],
              [
                {
                  columns: [
                    {
                      canvas: [
                        {
                          type: "rect",
                          x: 0,
                          y: 0,
                          w: 514,
                          h: 1,
                          color: "#F2F2F2",
                        },
                      ],
                    },
                  ],
                },
              ],
              [
                {
                  style: "tableExample",
                  layout: {
                    defaultBorder: false,
                  },
                  table: {
                    widths: [50, 60, 60, 200],
                    body: [
                      [
                        {
                          columns: [
                            {
                              image: "assets/money-pdf.png",
                              width: 10,
                              margin: [0, 3, 0, 0],
                            },
                            {
                              text: "ชำระเงิน",
                              style: "headercolone",
                              margin: [2, 0, 0, 0],
                            },
                          ],
                        },
                        { text: "วันที่ชำระ :" },
                        { text: invoiceData.invoice.date, alignment: "right" },
                        {
                          margin: [30, 0, 0, 0],
                          columns: [
                            {
                              image: "assets/money-pdf.png",
                              width: 10,
                              margin: [0, 3, 0, 0],
                            },
                            {
                              text: "เงินสด เงินสด",
                              style: "headercolone",
                              italics: true,
                              margin: [10, 0, 0, 0],
                            },
                          ],
                        },
                      ],
                      [
                        {},
                        { text: "จำนวนเงินรวม :" },
                        {
                          text: `${commaNumber(total)} บาท`,
                          alignment: "right",
                        },
                        {},
                      ],
                    ],
                  },
                },
              ],
              [
                {
                  columns: [
                    {
                      canvas: [
                        {
                          type: "rect",
                          x: 0,
                          y: 0,
                          w: 514,
                          h: 1,
                          color: "#F2F2F2",
                        },
                      ],
                    },
                  ],
                },
              ],
              [
                {
                  style: "tableExample",
                  layout: {
                    defaultBorder: false,
                  },
                  table: {
                    widths: [50, "*"],
                    body: [
                      [
                        {
                          columns: [
                            {
                              image: "assets/comment-pdf.png",
                              width: 10,
                              margin: [0, 3, 0, 0],
                            },
                            {
                              text: "หมายเหตุ",
                              style: "headercolone",
                              margin: [2, 0, 0, 0],
                            },
                          ],
                        },
                        { text: "", italics: true },
                      ],
                    ],
                  },
                },
              ],
            ],
          },
        },
      ];

      const docDefinition = {
        content: [
          {
            absolutePosition: { x: 410, y: 92 },
            canvas: [
              {
                type: "rect",
                x: 0,
                y: 0,
                w: 160,
                h: 70,
                color: "#F2F2F2",
              },
            ],
          },
          {
            columns: [
              {
                stack: [
                  {
                    image: "assets/logo-perm-suk-sap.png",
                    width: 40,
                    alignment: "left",
                  },
                ],
              },
              {
                stack: [
                  {
                    text: "(ต้นฉบับ)",
                    style: "typeinvoie",
                    alignment: "right",
                  },
                  {
                    text: "ใบเสร็จรับเงิน/ใบกำกับภาษี",
                    style: "textinvoie",
                    noWrap: true,
                  },
                ],
              },
            ],
          },
          {
            columns: [
              {
                margin: [0, 13, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        width: 45,
                        text: "ผู้ขาย :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text: "บริษัท เพิ่มพรฟูรณ์ จำกัด",
                        style: "headercolone",
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 45,
                        text: "ที่อยู่ :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text:
                          "เลขที่ 14 ซอย ประดิษฐ์มนูธรรม 27 แยก 8 แขวง\n" +
                          "ราชบุรณะ  บางบอน\n" +
                          "กรุงเทพมหานคร 10140",
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 45,
                        text: "เลขที่ภาษี :",
                        style: "headercolone",
                      },
                      {
                        width: 167,
                        text: "0105546044659 (สำนักงานใหญ่)",
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                ],
              },
              {
                margin: [17, 13, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        image: "assets/phone-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "0916159566",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        image: "assets/email-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "jirastudio.jira@gmail.com",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        image: "assets/host-pdf.png",
                        width: 6,
                        alignment: "left",
                        margin: [0, 4, 0, 0],
                      },
                      {
                        width: 120,
                        text: "https://jirastudioshop.com/",
                        style: "headercolone",
                        italics: true,
                        margin: [8, 0, 0, 0],
                      },
                    ],
                  },
                ],
              },
              {
                margin: [-45, 13, 0, 0],
                stack: [
                  {
                    columns: [
                      {
                        width: 60,
                        text: "เลขที่เอกสาร :",
                        style: "headercolone",
                      },
                      {
                        width: 90,
                        text: invoiceData.invoice.number,
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 60,
                        text: "วันที่ออก :",
                        style: "headercolone",
                      },
                      {
                        width: 120,
                        text: invoiceData.invoice.date,
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                  {
                    columns: [
                      {
                        width: 60,
                        text: "อ้างอิง :",
                        style: "headercolone",
                      },
                      {
                        width: 120,
                        text: "-",
                        style: "headercolone",
                        italics: true,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            style: "tableExample",
            layout: {
              defaultBorder: false,
            },
            table: {
              headerRows: 1,
              widths: [14, "*", 40, 60, 40, 20, 60],
              body: [
                [
                  { text: "คำอธิบาย", style: "tableHeader", colSpan: 2 },
                  {},
                  { text: "จำนวน", style: "tableHeader", alignment: "right" },
                  { text: "ราคา", style: "tableHeader", alignment: "right" },
                  { text: "ส่วนลด", style: "tableHeader", alignment: "right" },
                  { text: "VAT", style: "tableHeader", alignment: "right" },
                  {
                    text: "มูลค่าก่อนภาษี",
                    style: "tableHeader",
                    alignment: "right",
                  },
                ],
                ...invoiceData.invoice.items.map((item) => [
                  { text: item.numberno + ".", style: "tbody" },
                  {
                    text: `${item.description} : ${item.description}`,
                    italics: true,
                  },
                  {
                    text: item.quantity.toFixed(2),
                    alignment: "right",
                    style: "tbody",
                  },
                  {
                    text: commaNumber((item.price * item.quantity).toFixed(2)),
                    alignment: "right",
                    style: "tbody",
                  },
                  {
                    text: commaNumber((0).toFixed(2)),
                    alignment: "right",
                    style: "tbody",
                  },
                  { text: "7%", alignment: "right", style: "tbody" },
                  {
                    text: commaNumber(
                      priceBeforeTax(item.price * item.quantity)
                    ),
                    alignment: "right",
                    style: "tbody",
                  },
                ]),
              ],
            },
          },
          ...createContent,
        ],
        styles: {
          header: {
            fontSize: 14,
            bold: true,
            margin: [0, 10, 0, 10],
          },
          headercolone: {
            fontSize: 9,
          },
          tbody: {
            margin: [0, 3, 0, 3],
            italics: true,
          },
          subheader: {
            fontSize: 12,
            margin: [0, 5, 0, 5],
          },
          typeinvoie: {
            fontSize: 9,
            margin: [0, 0, 0, -5],
          },
          textinvoie: {
            fontSize: 26,
            margin: [-22, 0, 0, 0],
            color: "#848484",
          },
          tableHeader: {
            margin: [0, 5, 0, 5],
            fontSize: 10,
            color: "black",
            fillColor: "#F2F2F2",
          },
          tableExample: {
            fontSize: 9,
            margin: [0, 10, 0, 15],
          },
        },
        defaultStyle: {
          font: "Kanit",
        },
      };

      const pdfDoc = printer.createPdfKitDocument(docDefinition);
      let chunks = [];
      pdfDoc.on("data", (chunk) => {
        chunks.push(chunk);
      });
      pdfDoc.on("end", async () => {
        const thisBuffer = Buffer.concat(chunks);

        let pdfBuffer = h.response(thisBuffer).type("application/pdf");
        const uploadParams = {
          Bucket: "share",
          Key:
            "invoice_" +
            moment().format("DDMMYYYYSSS") +
            Math.random().toString(36).substring(2, 7) +
            "." +
            "pdf",
          Body: pdfBuffer.source,
          ContentType: pdfBuffer.headers["content-type"],
        };

        const upload = new Upload({
          client: S3,
          params: uploadParams,
        });

        const result = await upload.done();
        console.log("🚀 ~ pdfDoc.on ~ result:", result);
        if (typepay === 1) {
          await prisma.OrderTransactionTable.update({
            where: {
              id: +order_id,
            },
            data: {
              pdf_file: `https://${Constant.PATH_FILE}/${result.Key}`,
            },
          });
        } else {
          await prisma.OrderManualTransactionTable.update({
            where: {
              id: +order_id,
            },
            data: {
              pdf_file: `https://${Constant.PATH_FILE}/${result.Key}`,
            },
          });
        }
        let transporter = nodemailer.createTransport({
          service: "Gmail",
          host: "smtp.gmail.com",
          port: 465,
          secure: true,
          auth: {
            user: "chindanai.billy@gmail.com",
            pass: Constant.PASSEMAIL,
          },
        });

        let createSubject =
          "invoice_" +
          moment().format("DDMMYYYYSSS") +
          Math.random().toString(36).substring(2, 7);

        let createFilename =
          "invoice_" +
          moment().format("DDMMYYYYSSS") +
          Math.random().toString(36).substring(2, 7) +
          ".pdf";

        let mailOptions = {
          from: "chindanai.billy@gmail.com",
          to: email,
          subject: createSubject,
          text: "",
          attachments: [
            {
              filename: createFilename,
              content: thisBuffer,
            },
          ],
        };

        let mailOptionsForOwner = {
          from: "chindanai.billy@gmail.com",
          to: Constant.OWNER_EMAIL,
          subject: createSubject,
          text: "",
          attachments: [
            {
              filename: createFilename,
              content: thisBuffer,
            },
          ],
        };

        try {
          let info = await transporter.sendMail(mailOptions);
          let infoOwner = await transporter.sendMail(mailOptionsForOwner);
          console.log("Message sent: %s", info);
          console.log("Message sent: %s", infoOwner);
        } catch (error) {
          console.error(error);
        }
      });

      resolve({
        statusCode: 200,
      });
      pdfDoc.end();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

module.exports = {
  getCart,
  manageItemFromCart,
  removeItemFromCart,
  finishCart,
  finishCartQrCode,
  finishCartGuess,
  finishCartGuessQrCode,
  manualCreateOrder,
  uploadSlipManualOrder,
  confirmManualOrder,
  addStock,
  editStock,
  fullTax,
  shortTax,
};
