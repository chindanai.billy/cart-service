const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const Handler = require("./api.handler");
const Response = require("../utils/response");
const Boom = require("@hapi/boom");
const _ = require("underscore");
const moment = require("moment");
const { Upload } = require("@aws-sdk/lib-storage");
const S3 = require("../s3/s3.js");

const getCart = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload;
      const { id } = payload.data;
      console.log("🚀 ~ handler: ~ id:", id);

      const resultData = await Handler.getCart(id, prisma);
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const manageItemFromCart = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload;
      const {
        status_user,
        customer_table_id,
        ref_id,
        color,
        type,
        item_id,
        cart_id,
      } = payload.data;
      console.log("🚀 ~ handler: ~ payload.data:", payload.data);

      const resultData = await Handler.manageItemFromCart(
        status_user,
        customer_table_id,
        ref_id,
        color,
        type,
        item_id,
        cart_id,
        prisma
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      } else {
        return {
          statusCode: 200,
          result: customer_table_id,
        };
      }
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const removeItemFromCart = {
  auth: false,
  handler: async (request) => {
    try {
      const { id } = request.params;
      // console.log("🚀 ~ handler: ~ id:", id);

      const resultData = await Handler.removeItemFromCart(id, prisma);
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      } else {
        const findCart = await prisma.CartTable.findFirst({
          where: {
            id: +resultData.result.cart_table_id,
          },
        });

        return {
          statusCode: 200,
          result: findCart.customer_table_id,
        };
      }
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const finishCart = {
  auth: false,
  handler: async (request, h) => {
    try {
      const payload = request.payload;
      const { token, cart_table_id, payer_data, discount_code, tax_data } =
        payload.data;

      const resultData = await Handler.finishCart(
        token,
        cart_table_id,
        payer_data,
        discount_code,
        tax_data,
        prisma,
        h
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const finishCartQrCode = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload;
      const { slip, cart_table_id, payer_data, discount_code, tax_data } =
        payload.data;

      const resultData = await Handler.finishCartQrCode(
        slip,
        cart_table_id,
        payer_data,
        discount_code,
        tax_data,
        prisma
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const finishCartGuess = {
  auth: false,
  handler: async (request, h) => {
    try {
      const payload = request.payload;
      const {
        token,
        payer_data,
        discount_code,
        tax_data,
        cart_item,
        tex_status,
      } = payload.data;

      const resultData = await Handler.finishCartGuess(
        token,
        payer_data,
        discount_code,
        tax_data,
        cart_item,
        tex_status,
        prisma,
        h
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const finishCartGuessQrCode = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload;
      const {
        slip,
        payer_data,
        discount_code,
        tax_data,
        cart_item,
        tex_status,
      } = payload.data;

      const resultData = await Handler.finishCartGuessQrCode(
        slip,
        payer_data,
        discount_code,
        tax_data,
        cart_item,
        tex_status,
        prisma
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const manualCreateOrder = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload;
      console.log(
        "🚀 ~ file: api.controller.js:148 ~ handler: ~ order:",
        payload.data
      );

      const resultData = await Handler.manualCreateOrder(payload.data, prisma);
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const uploadSlipManualOrder = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload;

      const resultData = await Handler.uploadSlipManualOrder(
        payload.data,
        prisma
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const confirmManualOrder = {
  auth: false,
  handler: async (request, h) => {
    try {
      const payload = request.payload;

      const resultData = await Handler.confirmManualOrder(
        payload.data,
        prisma,
        h
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getManualOrder = {
  auth: false,
  handler: async (request) => {
    try {
      const data = request.payload.data;

      const count = await prisma.OrderManualTransactionTable.count({
        where: {
          transaction_id: { contains: data.search },
        },
      });
      const getManualOrder = await prisma.OrderManualTransactionTable.findMany({
        where: {
          transaction_id: { contains: data.search },
        },
        orderBy: {
          created_at: "desc",
        },
        select: {
          id: true,
          transaction_id: true,
          CartTable: {
            select: {
              id: true,
              CustomerTable: {
                select: {
                  id: true,
                  payer_information_table: {
                    select: {
                      id: true,
                      first_name: true,
                      last_name: true,
                    },
                  },
                },
              },
              PayerInformationGuestTable: {
                select: {
                  id: true,
                  first_name: true,
                  last_name: true,
                },
              },
            },
          },
          delivery_status: true,
          status: true,
          total_price: true,
          paid_price: true,
          created_order_at: true,
          paid_at: true,
          slip: true,
          created_at: true,
          updated_at: true,
        },
        skip: data.page === 1 ? 0 : data.take,
        take: data.take === 0 ? count : data.take,
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: { order_manual: getManualOrder, count: count },
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getOrder = {
  auth: false,
  handler: async (request) => {
    try {
      const data = request.payload.data;

      const count = await prisma.OrderTransactionTable.count({
        where: {
          transaction_id: { contains: data.search },
        },
      });
      const getOrder = await prisma.OrderTransactionTable.findMany({
        where: {
          transaction_id: { contains: data.search },
        },
        orderBy: {
          created_at: "desc",
        },
        select: {
          id: true,
          charge_id: true,
          transaction_id: true,
          CartTable: {
            select: {
              id: true,
              CustomerTable: {
                select: {
                  id: true,
                  payer_information_table: {
                    select: {
                      id: true,
                      first_name: true,
                      last_name: true,
                    },
                  },
                },
              },
              PayerInformationGuestTable: {
                select: {
                  id: true,
                  first_name: true,
                  last_name: true,
                },
              },
            },
          },
          delivery_status: true,
          status: true,
          total_price: true,
          paid_price: true,
          created_order_at: true,
          paid_at: true,
          created_at: true,
          updated_at: true,
        },
        skip: data.page === 1 ? 0 : data.take,
        take: data.take === 0 ? count : data.take,
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: { order: getOrder, count: count },
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getStock = {
  auth: false,
  handler: async (request) => {
    try {
      const data = request.payload.data;

      const count = await prisma.StockTable.count({
        where: {
          product_name: { contains: data.product_name },
        },
      });
      const getStock = await prisma.StockTable.findMany({
        where: {
          product_name: { contains: data.product_name },
        },
        orderBy: {
          created_at: "desc",
        },
        select: {
          id: true,
          product_code: true,
          product_name: true,
          qty: true,
          color_name: true,
          status: true,
          StockMovementTable: {
            orderBy: {
              created_at: "desc",
            },
            where: {
              OR: [
                {
                  status: 1,
                },
                {
                  status: 3,
                },
              ],
            },
            select: {
              id: true,
              qty: true,
              cost: true,
              change_qty: true,
              change_cost: true,
              status: true,
            },
            take: 1,
          },
        },
        skip: data.page === 1 ? 0 : data.take,
        take: data.take === 0 ? count : data.take,
      });

      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: { stock: getStock, count: count },
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const addStock = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload.data;
      const {
        product_id,
        product_code,
        product_name,
        color_name,
        color,
        status,
        qty_movement,
        cost,
        id_user,
      } = payload.data;

      const resultData = await Handler.addStock(
        product_id,
        product_code,
        product_name,
        color_name,
        color,
        status,
        qty_movement,
        cost,
        id_user,
        prisma
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const editStock = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload.data;
      const { stock_id, status, qty_movement, cost, id_user } = payload.data;

      const resultData = await Handler.editStock(
        stock_id,
        status,
        qty_movement,
        cost,
        id_user,
        prisma
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      return {
        ...resultData,
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getStockMovement = {
  auth: false,
  handler: async (request) => {
    try {
      const data = request.payload.data;
      console.log("🚀 ~ handler: ~ data:", data);

      const count = await prisma.StockMovementTable.count({
        where: {
          stock_table_id: data.id,
          ...(data.search !== ""
            ? { order_id: { contains: data.search } }
            : {}),
        },
      });
      const getStockMovement = await prisma.StockTable.findFirst({
        where: {
          id: data.id,
        },
        orderBy: {
          created_at: "desc",
        },
        select: {
          id: true,
          product_code: true,
          product_name: true,
          color_name: true,
          StockMovementTable: {
            where: {
              ...(data.search !== ""
                ? { order_id: { contains: data.search } }
                : {}),
            },
            orderBy: {
              created_at: "desc",
            },
            select: {
              id: true,
              stock_table_id: true,
              status: true,
              order_id: true,
              qty: true,
              cost: true,
              change_qty: true,
              change_cost: true,
              created_at: true,
              UserTable: {
                select: {
                  first_name: true,
                  last_name: true,
                },
              },
            },
            skip: data.page === 1 ? 0 : data.take,
            take: data.take === 0 ? count : data.take,
          },
        },
      });

      for (let order of getStockMovement.StockMovementTable) {
        if (order.order_id) {
          const orderT = await prisma.OrderTransactionTable.findFirst({
            where: {
              order_id: order.order_id,
            },
            select: {
              pdf_file: true,
            },
          });

          const orderM = await prisma.OrderManualTransactionTable.findFirst({
            where: {
              order_id: order.order_id,
            },
            select: {
              pdf_file: true,
            },
          });

          if (!_.isEmpty(orderT)) {
            order.order_link = orderT.pdf_file;
          }

          if (!_.isEmpty(orderM)) {
            order.order_link = orderM.pdf_file;
          }
        }
        order.created_at = moment(order.created_at).format("YYYY-MM-DD");
      }
      // if (_.isEmpty(getRole)) {
      //   return Response.errorMsg(500, "error");
      // }

      return {
        statusCode: 200,
        result: { stock: getStockMovement, count: count },
      };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const generateInv = {
  auth: false,
  handler: async (request) => {
    try {
      const payload = request.payload.data;

      // const resultData = await Handler.fullTax();
      // // const resultData = await Handler.shortTax();
      //   // if (resultData.statusCode !== 200) {
      //   //   return Response.errorMsg(resultData.statusCode, "");
      //   // }
      // let chunks = [];
      // resultData.result.on("data", (chunk) => {
      //   chunks.push(chunk);
      // });
      // resultData.result.on("end", async () => {
      //   const pdfBuffer = Buffer.concat(chunks);

      //   // return {
      //   //   ...resultData,
      //   // };

      //   let transporter = nodemailer.createTransport({
      //     service: "Gmail",
      //     host: "smtp.gmail.com",
      //     port: 465,
      //     secure: true,
      //     auth: {
      //       user: "chindanai.billy@gmail.com",
      //       pass: Constant.PASSEMAIL,
      //     },
      //   });

      //   let mailOptions = {
      //     from: "chindanai.billy@gmail.com",
      //     to: "chindanai.4107@gmail.com",
      //     subject: "This is Tax",
      //     text: "Tax",
      //     // html: "<b>Hello world?</b>",
      //     attachments: [
      //       {
      //         filename: "tax.pdf",
      //         content: pdfBuffer,
      //       },
      //     ],
      //   };

      //   try {
      //     let info = await transporter.sendMail(mailOptions);
      //     console.log("Message sent: %s", info);
      //     // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      //   } catch (error) {
      //     console.error(error);
      //   }
      // });

      // resultData.result.end();
      return "";
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

const getPurchaseHistory = {
  auth: false,
  handler: async (request, h) => {
    try {
      const data = request.payload.data;

      // const findCart = await prisma.CartTable.findMany({
      //   orderBy: {
      //     created_at: "desc",
      //   },
      //   where: {
      //     customer_table_id: +data.id,
      //   },
      //   select: {
      //     id: true,
      //     CartItemTable: {
      //       select: {
      //         id: true,
      //         ref_id: true,
      //         qty: true,
      //         metadata: true,
      //       },
      //     },
      //     OrderTransactionTable: {
      //       select: {
      //         id: true,
      //         order_id: true,
      //         charge_id: true,
      //         transaction_id: true,
      //         delivery_status: true,
      //         status: true,
      //         total_price: true,
      //         paid_price: true,
      //         created_order_at: true,
      //         paid_at: true,
      //       },
      //     },
      //     OrderManualTransactionTable: {
      //       select: {
      //         id: true,
      //         order_id: true,
      //         transaction_id: true,
      //         delivery_status: true,
      //         status: true,
      //         total_price: true,
      //         paid_price: true,
      //         created_order_at: true,
      //         paid_at: true,
      //       },
      //     },
      //   },
      // });

      // let resultType1 = findCart.filter(
      //   (item) => item.OrderTransactionTable.length !== 0
      // );

      // const findCartManual = await prisma.CartTable.findMany({
      //   orderBy: {
      //     created_at: "desc",
      //   },
      //   where: {
      //     customer_table_id: +data.id,
      //   },
      //   select: {
      //     id: true,
      //     CartItemTable: {
      //       select: {
      //         id: true,
      //         ref_id: true,
      //         qty: true,
      //         metadata: true,
      //       },
      //     },
      //     OrderManualTransactionTable: {
      //       select: {
      //         id: true,
      //         order_id: true,
      //         transaction_id: true,
      //         delivery_status: true,
      //         status: true,
      //         total_price: true,
      //         paid_price: true,
      //         created_order_at: true,
      //         paid_at: true,
      //       },
      //     },
      //   },
      // });
      // let resultType2 = findCartManual.filter(
      //   (item) => item.OrderManualTransactionTable.length !== 0
      // );

      const resultData = await Handler.fullTax(
        data.name,
        data.address,
        data.tax_id,
        data.phone,
        data.number_order,
        data.date_order,
        data.item,
        data.email
      );
      if (resultData.statusCode !== 200) {
        return Response.errorMsg(resultData.statusCode, resultData.result);
      }

      const updatePdf = async () => {
        return await prisma.$transaction(async (tx) => {
          const findOrder = await tx.OrderTransactionTable.findFirst({
            where: {},
          });
        });
      };

      await updatePdf();

      return {
        ...resultData,
      };

      // return {
      //   statusCode: 200,
      //   result: { typeOne: resultType1, typeTwo: resultType2 },
      // };
    } catch (error) {
      console.log(error);
      return Boom.badImplementation();
    }
  },
};

module.exports = {
  getCart,
  manageItemFromCart,
  removeItemFromCart,
  finishCart,
  finishCartQrCode,
  finishCartGuess,
  finishCartGuessQrCode,
  manualCreateOrder,
  uploadSlipManualOrder,
  confirmManualOrder,
  getManualOrder,
  getOrder,
  getStock,
  addStock,
  editStock,
  getStockMovement,
  generateInv,
  getPurchaseHistory,
};
