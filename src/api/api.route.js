const Controller = require("./api.controller");

const routes = [
  {
    method: "POST",
    path: "/cart-service/get-cart",
    config: Controller.getCart,
  },
  {
    method: "POST",
    path: "/cart-service/manage-item-from-cart",
    config: Controller.manageItemFromCart,
  },
  {
    method: "DELETE",
    path: "/cart-service/remove-item-from-cart/{id}",
    config: Controller.removeItemFromCart,
  },
  {
    method: "POST",
    path: "/cart-service/finish-cart",
    config: Controller.finishCart,
  },
  {
    method: "POST",
    path: "/cart-service/finish-cart-guess",
    config: Controller.finishCartGuess,
  },
  {
    method: "POST",
    path: "/cart-service/manual-create-order",
    config: Controller.manualCreateOrder,
  },
  {
    method: "PUT",
    path: "/cart-service/upload-slip-manual-order",
    config: Controller.uploadSlipManualOrder,
  },
  {
    method: "PUT",
    path: "/cart-service/confirm-manual-order",
    config: Controller.confirmManualOrder,
  },
  {
    method: "POST",
    path: "/cart-service/get-manual-order",
    config: Controller.getManualOrder,
  },
  {
    method: "POST",
    path: "/cart-service/get-order",
    config: Controller.getOrder,
  },
  {
    method: "POST",
    path: "/cart-service/get-stock",
    config: Controller.getStock,
  },
  {
    method: "POST",
    path: "/cart-service/add-stock",
    config: Controller.addStock,
  },
  {
    method: "PUT",
    path: "/cart-service/edit-stock",
    config: Controller.editStock,
  },
  {
    method: "POST",
    path: "/cart-service/get-stock-movement",
    config: Controller.getStockMovement,
  },
  {
    method: "POST",
    path: "/cart-service/generate-inv",
    config: Controller.generateInv,
  },
  {
    method: "POST",
    path: "/cart-service/get-purchase-history",
    config: Controller.getPurchaseHistory,
  },
  {
    method: "POST",
    path: "/cart-service/finish-cart-qr-code",
    config: Controller.finishCartQrCode,
  },
  {
    method: "POST",
    path: "/cart-service/finish-cart-guess-qr-code",
    config: Controller.finishCartGuessQrCode,
  },
];

module.exports = routes;
