const Constant = require("../constant");
const { S3Client } = require("@aws-sdk/client-s3");

const S3 = new S3Client({
  region: "auto",
  endpoint: `https://${Constant.R2_ACCOUNT_ID}.r2.cloudflarestorage.com`,
  credentials: {
    accessKeyId: Constant.R2_ACCESS_KEY,
    secretAccessKey: Constant.R2_SECRET_KEY,
  },
});

module.exports = S3;
