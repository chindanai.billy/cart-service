require("dotenv").config();

const Constant = {
  HOST: process.env.HOST,
  PORT: process.env.PORT,
  JWT_SECRET: process.env.JWT_SECRET_KEY,
  SECRET_KEY: process.env.SECRET_KEY,
  MINUS: "MINUS",
  PASSEMAIL: process.env.PASSEMAIL,
  R2_ACCESS_KEY: process.env.R2_ACCESS_KEY,
  R2_ACCOUNT_ID: process.env.R2_ACCOUNT_ID,
  R2_SECRET_KEY: process.env.R2_SECRET_KEY,
  PATH_FILE: process.env.PATH_FILE,
  OWNER_EMAIL: process.env.OWNER_EMAIL
};

module.exports = Constant;
