const glob = require('glob')
const log = require('./utils/log')

exports.plugin = {
  name: 'router',
  version: '1.0.0',
  register: (server, options) => {
    const files = glob.sync('./**/*.route.js', {
      absolute: true
    })

    log.info('Path the following...')
    /* Inject Routing */
    files.map((file) => {
      const routes = require(file)
      routes.forEach((route) => log.info(route.method + ' ' + route.path))
      server.route(routes)
    })
  }
}
